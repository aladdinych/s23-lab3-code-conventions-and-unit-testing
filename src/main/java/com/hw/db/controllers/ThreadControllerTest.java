package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;


public class ThreadControllerTest {

    private Thread thread;
    private Thread secondThread;
    private ThreadController threadController;
    private Post post;

    @BeforeEach
    void beforeEach() {
        // Create a test thread with varied properties
        thread = new Thread("test-thread-1", new Timestamp(123456L), "test-forum-1", "test-message-1", "test-slug-1", "test-title-1", 5);
        thread.setId(1);

        // Create a second test thread with even more varied properties
        secondThread = new Thread("test-thread-2", new Timestamp(1234567L), "test-forum-2", "test-message-2", "test-slug-2", "test-title-2", 15);
        secondThread.setId(2);

        // Create a test post with a varied message
        post = new Post();
        post.setMessage("This is a test post message!");

        // Create a ThreadController to control all the threads
        threadController = new ThreadController();
    }


    @Test
    @DisplayName("Successfully create post in thread")
    void correctlyCreatePost() {
        // Arrange
        MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
        userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());

        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);

        // Act
        ResponseEntity result = threadController.createPost("test-slug-1", List.of(post));

        // Assert
        assertEquals(result.getStatusCode(), HttpStatus.CREATED);
        assertEquals(result.getBody(), List.of(post));

        // Clean up
        userMock.close();
        threadDao.close();
    }

    @Test
    @DisplayName("Successfully get posts")
    void correctlyGetPosts() {
        // Arrange
        MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
        userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());

        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
        threadDao.when(() -> ThreadDAO.getPosts(any(), any(), any(), any(), any())).thenReturn(List.of(post));

        // Act
        ResponseEntity result = threadController.Posts("test-slug-1", 1, 2, "asc", true);

        // Assert
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(result.getBody(), List.of(post));

        // Clean up
        userMock.close();
        threadDao.close();
    }

    @Test
    @DisplayName("Successfully change thread information")
    void correctlyChangeThreadInfo() {
        // Arrange
        MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
        userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());

        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
        threadDao.when(() -> ThreadDAO.getThreadById(anyInt())).thenReturn(secondThread);

        // Act
        ResponseEntity result = threadController.change("test-slug-1", secondThread);

        // Assert
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(result.getBody(), secondThread);

        // Clean up
        userMock.close();
        threadDao.close();
    }

    @Test
    @DisplayName("Successfully get thread information")
    void correctlyGetThreadInfo() {
        // Arrange
        MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
        userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());

        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);

        // Act
        ResponseEntity result = threadController.info("test-slug-1");

        // Assert
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(result.getBody(), thread);

        // Clean up
        userMock.close();
        threadDao.close();
    }

    @Test
    @DisplayName("Successfully create vote")
    void correctlyCreateVote() {
        // Arrange
        MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
        userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());

        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
        threadDao.when(() -> ThreadDAO.change(any(), anyInt())).thenReturn(10);

        int voteBefore = thread.getVotes();

        // Act
        ResponseEntity result = threadController.createVote("test-slug-1", new Vote("Daniil", 100));

        // Assert
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(result.getBody(), thread);

        int voteAfter = thread.getVotes();
        assertEquals(voteAfter, voteBefore + 100);

        // Clean up
        userMock.close();
        threadDao.close();
    }
}